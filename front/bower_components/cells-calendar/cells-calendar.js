/* global moment, _ */

(function() {
  'use strict';

  const selectedClass = 'selected';
  /**
   * @customElement
   * @polymer
   * @demo demo/index.html
   */
  Polymer({

    is: 'cells-calendar',

    /**
     * Event fired when a object is selected in the combo with the selected object
     *
     * @event combo-value-changed
     *
     */
    /**
     * Event fired when a day is selected with the day selected as unix timestamp
     *
     * @event day-selected
     *
     */
    /**
     * Event fired when a month is selected with the first day of the month selected as unix timestamp
     *
     * @event month-selected
     *
     */
    /**
     * Event fired when a year is selected with the first day of the year selected as unix timestamp
     *
     * @event year-selected
     *
     */
    /**
     * Event fired when the month view is changed with the first day of the month being viewed as unix timestamp
     *
     * @event month-changed
     *
     */
    /**
     * Event fired when the year view is changed with the first day of the year being viewed as unix timestamp
     *
     * @event year-changed
     *
     */
    /**
     * Event fired when the year range view is changed with the first day of the year displayed in center of screen as timestamp
     *
     * @event years-range-changed
     *
     */

    behaviors: [
      window.CellsBehaviors.i18nBehavior,
    ],

    properties: {

      /**
       * Initial date to show in calendar. It will display the Days view of the
       * month/year informed. (Should be informed as unix timestamp).
       *
       * @default current day
       */
      initialDate: {
        type: Date,
        value: function() {
          return new Date().getTime();
        },
      },

      /**
       * Value to define the initial view of the calendar
       * (Available values are 'Days', 'Months', 'Years').
       */
      view: {
        type: String,
        value: 'Days',
      },

      /**
       * Date that should be selected when calendar opens. (Should be informed as unix timestamp)
       */
      selectedDate: {
        type: Date,
      },

      /**
       * Array of items to be displayed in the combo box. (Should
       * follow the pattern {label: 'label', value: 'value'}). If not informed, the combo will not be showed
       */
      comboItems: {
        type: Array,
        value: function() {
          return [];
        },
      },

      /**
       * Value of an item informed in combo-items array to be selected by default when calendar opens.
       * Will not take effect if combo-items is not informed
       */
      selectedComboItem: {
        type: Object,
      },

      /**
       * Array of unix timestamps that should be highlight in the calendar
       */
      highlightDays: {
        type: Array,
        value: function() {
          return [];
        },
      },

      /**
       * Object to define the year range to show in the years range view.
       *
       * @type {Number}
       */
      yearsViewCount: {
        type: Number,
        value: 28,
      },

      /**
       * Icon for the previous button in the controls section
       */
      prevIcon: {
        type: String,
        value: '',
      },

      /**
       * Icon for the next button in the controls section
       */
      nextIcon: {
        type: String,
        value: '',
      },

      /**
       * Icon for the combo button in the controls section
       */
      comboIcon: {
        type: String,
        value: '',
      },

      /**
       * Locale used to define first day of week (Available values are 'es' or 'en')
       */
      locale: {
        type: String,
        value: () => I18nMsg.lang,
      },

      now: {
        type: Object,
      },
    },

    observers: [
      '_initialization(initialDate, locale, initialized)',
      '_render(now, view, highlightDays)',
      '_dateSelectedChanged(selectedDate, initialized)',
    ],

    created: function() {
      this.views = {
        Days: {
          item: 'day',
          heading: 'months',
        },
        Months: {
          item: 'month',
          heading: 'year',
        },
        Years: {
          item: 'year',
          heading: 'years',
        },
      };
    },

    attached: function() {
      //Setting initial moment locale based on CellsBehaviors.i18nBehavior overriding  default browser behavior
      moment.locale(I18nMsg.lang);
      document.addEventListener('i18n-lang-changed', this._setLocale.bind(this));
    },

    detached: function() {
      document.addEventListener('i18n-lang-changed', this._setLocale.bind(this));
    },

    ready: function() {
      this.initialized = true;
    },

    _setLocale: function(locale) {
      this.set('locale', locale.detail.language);
    },

    _initialization: function() {
      this.updateNow(this.initialDate);
    },

    _showCombo: function() {
      return this.comboItems.length !== 0;
    },

    _comboValueChanged: function(event) {
      if (event.detail.value) {
        this.dispatchEvent(new CustomEvent('combo-value-changed', {
          bubbles: true,
          composed: true,
          detail: event.detail.value,
        }));
      }
    },

    _dateSelectedChanged: function(selectedDate) {
      // If there are no selected class yet, it means selectedDate
      // was set as init param (not by click) and then we need to
      // paint it 'manually'
      if (!this.$$('.selected')) {
        this._setDays();
      }

      this.dispatchEvent(new CustomEvent('day-selected', {
        bubbles: true,
        composed: true,
        detail: selectedDate,
      }));
    },

    /**
     * Render the view with the input data informed/changed
     * It is responsible to change view, highlight days, etc...
     */
    _render: function() {
      this['_set' + this.view]();
    },

    /**
     * Go to the previous month, year or year range
     */
    _prev: function() {
      this.now = this.now.clone().subtract(1, this.views[this.view].heading);
      this['_change' + this.view]();
    },

    /**
     * Go to the next month, year or year range
     */
    _next: function() {
      this.now = this.now.clone().add(1, this.views[this.view].heading);
      this['_change' + this.view]();
    },

    _changeDays: function() {
      var selectedDay = moment(this.now, 'D MMMM YYYY');
      this.dispatchEvent(new CustomEvent('month-changed', {
        bubbles: true,
        composed: true,
        detail: selectedDay.startOf('month').valueOf(),
      }));
    },

    _changeMonths: function() {
      var selectedDay = moment(this.now, 'D MMMM YYYY');
      this.dispatchEvent(new CustomEvent('year-changed', {
        bubbles: true,
        composed: true,
        detail: selectedDay.startOf('year').valueOf(),
      }));
    },

    _changeYears: function() {
      var selectedDay = moment(this.now, 'D MMMM YYYY');
      this.dispatchEvent(new CustomEvent('years-range-changed', {
        bubbles: true,
        composed: true,
        detail: selectedDay.startOf('year').valueOf(),
      }));
    },

    /**
     * Go to the next calendar view
     */
    _nextView: function() {
      var keys = Object.keys(this.views);
      var view = keys[keys.indexOf(this.view) + 1];
      this.view = view;
    },

    /**
     * Go to the previous calendar view
     */
    _prevView: function() {
      var keys = Object.keys(this.views);
      var view = keys[keys.indexOf(this.view) - 1];
      this.view = view;
    },

    _selectedMonths: function(selectedDay) {
      var date = selectedDay.startOf('month').valueOf();
      this.now = selectedDay;
      this.dispatchEvent(new CustomEvent('month-selected', {
        bubbles: true,
        composed: true,
        detail: date,
      }));
      this._prevView();
    },

    _selectedYears: function(selectedDay) {
      var date = selectedDay.startOf('year').valueOf();
      this.now = selectedDay;
      this.dispatchEvent(new CustomEvent('year-selected', {
        bubbles: true,
        composed: true,
        detail: date,
      }));
      this._prevView();
    },

    _selectedDays: function(selectedDay, event) {
      // Move selected class from the old selection to the target clicked
      let dom = Polymer.Element ? this.shadowRoot : Polymer.dom(this.root);
      if (dom.querySelector('.selected')) {
        dom.querySelector('.selected').classList.remove(selectedClass);
      }
      Polymer.dom(event.currentTarget).classList.add(selectedClass);
      this.selectedDate = selectedDay.startOf('day').valueOf();
    },

    _setItem: function(event) {
      if (event.target.className.indexOf('active') === -1) {
        return;
      }

      var selection = event.model.get('calItem.val');
      var selectedDay = moment(selection, 'YYYYMMDD');

      this['_selected' + this.view](selectedDay, event);
    },

    _setDays: function() {
      var start = this.now.clone().startOf('month');
      var end = this.now.clone().endOf('month');
      var items = this.items = [];
      var month = this.now.month();

      this.weekdays = ['cells-calendar-sunday-min', 'cells-calendar-monday-min',
        'cells-calendar-tuesday-min', 'cells-calendar-wednesday-min',
        'cells-calendar-thursday-min', 'cells-calendar-friday-min',
        'cells-calendar-saturday-min'];

      // move the array according to the first day of week for the set locale
      var currentLocaleData = moment.localeData();
      this.weekdays.push.apply(this.weekdays, this.weekdays.splice(0, currentLocaleData.firstDayOfWeek()));

      this.type = 'days';

      this.controlsPrimaryTitle = 'cells-calendar-month-' + this.now.format('M') + '-short';
      this.controlsSecondaryTitle = this.now.format('YY');

      // Fill the items array with empty elements until we reach first day of week for start month
      for (var i = 0; i < start.weekday(); i++) {
        items.push({
          val: '',
          label: '',
          cl: 'element hiddenDay',
        });
      }

      // Fill the days in the calendar
      while (start.isBefore(end) || start.isSame(end)) {
        var obj = this._getDayClass('element active', start.format('YYYYMMDD'));
        items.push({
          val: start.format('YYYYMMDD'),
          label: start.format('D'),
          cl: obj.cssClass,
          tooltip: obj.tooltip,
          type: this.type,
        });
        start.add(1, 'days');
      }

      // Fill the remaining items array with empty elements until we fill all calendar
      var cf = (items.length > 35) ? (41 - (items.length)) : (34 - (items.length));
      for (var j = 0; j <= cf; j++) {
        items.push({
          val: '',
          label: '',
          cl: 'element hiddenDay',
        });
      }

      this.calItems = items;
    },

    _getDayClass: function(cssClass, date) {
      var aux = moment(date, 'YYYYMMDD');
      if (aux.isSame(moment(), 'day')) {
        cssClass += ' today';
      }

      cssClass = this._getElementClass(cssClass, date, 'day');

      if (this.highlightDays.length > 0) {
        var wrapped = _(this.highlightDays);
        var other = wrapped.findIndex(function(o) {
          return moment(Number(o.day ? o.day : o)).isSame(aux, 'day');
        });
        if (other >= 0) {
          cssClass += ' day-highlighted-class';
          var tooltip = this.highlightDays[other].tooltip;
        }
      }

      return { cssClass: cssClass, tooltip: tooltip, };
    },

    _setMonths: function() {
      var start = this.now.clone().startOf('year');
      var end = this.now.clone().endOf('year');
      var items = this.items = [];
      this.type = 'months';

      this.weekdays = [];
      this.controlsPrimaryTitle = this.now.format('YYYY');
      this.controlsSecondaryTitle = '';

      while (start.isBefore(end) || start.isSame(end)) {
        items.push({
          val: start.format('YYYYMMDD'),
          label: 'cells-calendar-month-' + start.format('M') + '-long',
          cl: this._getElementClass('element active', start.format('YYYYMMDD'), 'month'),
          type: this.type,
        });
        start.add(1, 'months');
      }

      this.calItems = items;
    },

    _getElementClass: function(cssClass, date, elementType) {
      var aux = moment(date, 'YYYYMMDD');
      if (this.selectedDate && aux.isSame(moment(Number(this.selectedDate)), elementType)) {
        cssClass += ' ' + selectedClass;
      }

      return cssClass;
    },

    _setYears: function() {
      var yearStart = (this.yearsViewCount / 2) - 1;
      var yearEnd = (this.yearsViewCount / 2);

      var start = this.now.clone().subtract(yearStart, 'year');
      var end = this.now.clone().add(yearEnd, 'year');
      var items = this.items = [];
      this.type = 'years';

      this.weekdays = [];
      this.controlsPrimaryTitle = [start.format('YYYY'), end.format('YYYY')].join('-');
      this.controlsSecondaryTitle = '';

      while (start.isBefore(end) || start.isSame(end)) {
        items.push({
          val: start.format('YYYYMMDD'),
          label: start.format('YYYY'),
          cl: this._getElementClass('element active', start.format('YYYYMMDD'), 'year'),
          type: this.type,
        });
        start.add(1, 'years');
      }

      this.calItems = items;
    },

    /**
     * Function that can update the variable @see {now}
     * to be able to force a refresh of the calendar
     * @param {Number} timestamp - The new now moment in timestamp
     */
    updateNow: function(timestamp) {
      /* istanbul ignore else */
      if (timestamp) {
        moment.locale(this.locale);

        var now = moment(Number(timestamp)).startOf('day');
        if (!now.isValid()) {
          now = moment().startOf('day');
        }

        this.now = now;
      }
    },
  });
}());
