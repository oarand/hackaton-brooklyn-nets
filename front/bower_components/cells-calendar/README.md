![cells calendar screenshot](cells-calendar.jpg)

![Polymer Hybrid (1 - 2)](https://img.shields.io/badge/Polymer-1%20--%202-yellow.svg)

![Certificated](https://img.shields.io/badge/certificated-yes-brightgreen.svg)

[Demo of component in Cells Catalog](https://au-bbva-ether-cellscatalogs.appspot.com/?view=demo#/component/cells-calendar)

# cells-calendar

`cells-calendar` generates a calendar where selection of days, months and years generate an event with the selected
date. It can highlight days based on an array of dates (in unix timestamp) informed as input data.
It is also possible to include a combo box with a customized list that generate an event when one of the items is selected,
returning the object item selected.
It uses vaadin-combo-box (http://vaadin.com/elements/-/element/vaadin-combo-box) component to generate the combo-box.

## Usage

`cells-calendar` have most of the settings with a default value so its usage is very simple, only is need to inform
the icons using a iconset. When no initial date or selected date is informed, it will display the current month by default.
Also when no combo box list is informed, the combo will not be visible in the screen.
It is prepared to handle locales 'es' and 'en', including string translations. The first day of week is locale dependent and
this locale is informed as an attribute (which is indepdenty of the language selected. It means you can set locale 'es' to have
first day of week as Monday and display the texts in English)

#### Examples:

__Basic Example:__
```html
<cells-calendar prev-icon="coronita:back" next-icon="coronita:forward" combo-icon="coronita:unfold" ></cells-calendar>
```

Include a combobox with pre-selected element example:
```html
<cells-calendar prev-icon="coronita:back" next-icon="coronita:forward" combo-icon="coronita:unfold"
 combo-items="[[comboItems]]" selected-combo-item="[[selectedComboItem]]"></cells-calendar>
```

__Highlight days (informed in a array of timestamps) and set initial date to be a specific month so the calendar
is opened in the informed month example:__
```html
<cells-calendar prev-icon="coronita:back" next-icon="coronita:forward" combo-icon="coronita:unfold"
 highlight-days="[[highlightDays]]" initial-date="[[new Date(2015,1,1)]]"></cells-calendar>
```

__Initialize with a selected day informed example:__
```html
<cells-calendar prev-icon="coronita:back" next-icon="coronita:forward" combo-icon="coronita:unfold"
 selected-date="[[new Date(2015,1,1)]]"></cells-calendar>
```

__Initialized in Years view (possible values are 'Days, Months or Years')__
```html
<cells-calendar prev-icon="coronita:back" next-icon="coronita:forward" combo-icon="coronita:unfold"
 view="Years"></cells-calendar>
```

__Also can define calendar´s locale to be 'es' so the initial day of the week will be Monday.
At the moment only 'en' and 'es' are supported example:__
```html
<cells-calendar prev-icon="coronita:back" next-icon="coronita:forward" combo-icon="coronita:unfold"
 locale="es"></cells-calendar>
```

## Icons

Since this component uses icons, it will need an [iconset](https://bbva.cellsjs.com/guides/best-practices/cells-icons.html) in your project as an [application level dependency](https://bbva.cellsjs.com/guides/advanced-guides/application-level-dependencies.html). In fact, this component uses an iconset in its demo.

## Styling

The following custom properties and mixins are available for styling:

### Custom Properties
| Custom Property                                              | Selector                                                                                    | CSS Property                  | Value                                                                       |
| ------------------------------------------------------------ | ------------------------------------------------------------------------------------------- | ----------------------------- | --------------------------------------------------------------------------- |
| --cells-fontDefault                                          | :host                                                                                       | font-family                   |  sans-serif                                                                 |
| --cells-calendar-container-box-shadow                        | .container                                                                                  | box-shadow                    |  0 1px 4px 0 rgba(0, 0, 0, 0.2)                                             |
| --cells-calendar-container-height                            | .container                                                                                  | height                        |  310px                                                                      |
| --cells-calendar-container-margin                            | .container                                                                                  | margin                        |  auto                                                                       |
| --cells-calendar-container-max-width                         | .container                                                                                  | max-width                     |  288px                                                                      |
| --cells-calendar-header-border-bottom                        | .header                                                                                     | border-bottom                 |  1px solid ![#E9E9E9](https://placehold.it/15/E9E9E9/000000?text=+) #E9E9E9 |
| --cells-calendar-combo-box-input-container-padding           | .header .combo vaadin-combo-box-light.combo-box > --paper-input-container:                  | padding                       |  0 0 0 10px                                                                 |
| --cells-calendar-combo-box-input-container-input-font-family | .header .combo vaadin-combo-box-light.combo-box > --paper-input-container-input:            | font-family                   |  sans-serif                                                                 |
| --cells-calendar-combo-box-input-container-input-font-size   | .header .combo vaadin-combo-box-light.combo-box > --paper-input-container-input:            | font-size                     |  15px                                                                       |
| --cells-calendar-combo-box-input-color                       | .header .combo vaadin-combo-box-light.combo-box                                             | --paper-input-container-color |  ![#121212](https://placehold.it/15/121212/000000?text=+) #121212           |
| --cells-calendar-combo-box-button-color                      | .header .combo vaadin-combo-box-light.combo-box .toggle-button > --paper-icon-button:       | color                         |  ![#2A86CA](https://placehold.it/15/2A86CA/000000?text=+) #2A86CA           |
| --cells-calendar-combo-box-button-hover-color                | .header .combo vaadin-combo-box-light.combo-box .toggle-button > --paper-icon-button-hover: | color                         |  ![#5BBEFF](https://placehold.it/15/5BBEFF/000000?text=+) #5BBEFF           |
| --cells-calendar-combo-box-button-bg-color                   | .header .combo vaadin-combo-box-light.combo-box .toggle-button                              | --paper-icon-button-ink-color |  transparent                                                                |
| --cells-calendar-header-controls-height                      | .header .controls                                                                           | height                        |  54px                                                                       |
| --cells-calendar-header-controls-prev-button-color           | .header .controls .prev-btn > --paper-icon-button:                                          | color                         |  ![#2A86CA](https://placehold.it/15/2A86CA/000000?text=+) #2A86CA           |
| --cells-calendar-header-controls-prev-button-hover-color     | .header .controls .prev-btn > --paper-icon-button-hover:                                    | color                         |  ![#5BBEFF](https://placehold.it/15/5BBEFF/000000?text=+) #5BBEFF           |
| --cells-calendar-header-controls-prev-button-bg-color        | .header .controls .prev-btn                                                                 | --paper-icon-button-ink-color |  transparent                                                                |
| --cells-calendar-header-controls-title-cursor                | .header .controls .title                                                                    | cursor                        |  pointer                                                                    |
| --cells-calendar-header-controls-title-font-size             | .header .controls .title                                                                    | font-size                     |  13px                                                                       |
| --cells-calendar-header-controls-title-font-weight           | .header .controls .title                                                                    | font-weight                   |  500                                                                        |
| --cells-calendar-header-controls-title-text-align            | .header .controls .title                                                                    | text-align                    |  center                                                                     |
| --cells-calendar-header-controls-title-text-transform        | .header .controls .title                                                                    | text-transform                |  uppercase                                                                  |
| --cells-calendar-header-controls-next-button-color           | .header .controls .next-btn > --paper-icon-button:                                          | color                         |  ![#2A86CA](https://placehold.it/15/2A86CA/000000?text=+) #2A86CA           |
| --cells-calendar-header-controls-next-button-hover-color     | .header .controls .next-btn > --paper-icon-button-hover:                                    | color                         |  ![#5BBEFF](https://placehold.it/15/5BBEFF/000000?text=+) #5BBEFF           |
| --cells-calendar-header-controls-next-button-bg-color        | .header .controls .next-btn                                                                 | --paper-icon-button-ink-color |  transparent                                                                |
| --cells-calendar-main-padding-left                           | .main                                                                                       | padding-left                  |  5px                                                                        |
| --cells-calendar-main-padding-right                          | .main                                                                                       | padding-right                 |  5px                                                                        |
| --cells-calendar-main-element-cursor                         | .main .element                                                                              | cursor                        |  pointer                                                                    |
| --cells-calendar-main-element-height                         | .main .element                                                                              | height                        |  23px                                                                       |
| --cells-calendar-main-element-font-size                      | .main .element                                                                              | font-size                     |  13px                                                                       |
| --cells-calendar-main-element-line-height                    | .main .element                                                                              | line-height                   |  23px                                                                       |
| --cells-calendar-main-element-margin                         | .main .element                                                                              | margin                        |  6px 7px                                                                    |
| --cells-calendar-main-element-width                          | .main .element                                                                              | width                         |  23px                                                                       |
| --cells-calendar-main-element-transition                     | .main .element                                                                              | transition                    |  font-weight 0.3s ease-out                                                  |
| --cells-calendar-main-element-days-border-radius             | .main .element.days                                                                         | border-radius                 |  50%                                                                        |
| --cells-calendar-main-element-days-width                     | .main .element.days                                                                         | width                         |  23px                                                                       |
| --cells-calendar-main-element-days-today-bg-color            | .main .element.today                                                                        | background-color              |  transparent                                                                |
| --cells-calendar-main-element-days-today-border              | .main .element.today                                                                        | border                        |  1px solid ![#2a86cA](https://placehold.it/15/2a86cA/000000?text=+) #2a86cA |
| --cells-calendar-main-element-days-width                     | .main .element.hiddenDay                                                                    | width                         |  23px                                                                       |
| --cells-calendar-main-element-months-border-radius           | .main .element.months                                                                       | border-radius                 |  4px                                                                        |
| --cells-calendar-main-element-months-margin                  | .main .element.months                                                                       | margin                        |  18px auto                                                                  |
| --cells-calendar-main-element-months-width                   | .main .element.months                                                                       | width                         |  80px                                                                       |
| --cells-calendar-main-element-years-border-radius            | .main .element.years                                                                        | border-radius                 |  4px                                                                        |
| --cells-calendar-main-element-margin                         | .main .element.years                                                                        | margin                        |  3px 6px                                                                    |
| --cells-calendar-main-element-years-width                    | .main .element.years                                                                        | width                         |  60px                                                                       |
| --cells-calendar-main-element-day-highlighted-font-weight    | .main .element.day-highlighted-class                                                        | font-weight                   |  700                                                                        |
| --cells-calendar-main-element-selected-bg-color              | .main .element.selected                                                                     | background-color              |  ![#5bbeff](https://placehold.it/15/5bbeff/000000?text=+) #5bbeff           |
| --cells-calendar-main-element-selected-color                 | .main .element.selected                                                                     | color                         |  ![#ffffff](https://placehold.it/15/ffffff/000000?text=+) #ffffff           |
| --cells-calendar-main-element-selected-font-weight           | .main .element.selected                                                                     | font-weight                   |  700                                                                        |
| --cells-calendar-main-element-selected-hover-bg-color        | .main .element.selected:hover                                                               | background-color              |  ![#5bbeff](https://placehold.it/15/5bbeff/000000?text=+) #5bbeff           |
| --cells-calendar-main-element-selected-hover-border          | .main .element.selected:hover                                                               | border                        |  1px solid ![#2a86cA](https://placehold.it/15/2a86cA/000000?text=+) #2a86cA |
| --cells-calendar-main-element-selected-hover-color           | .main .element.selected:hover                                                               | color                         |  ![#ffffff](https://placehold.it/15/ffffff/000000?text=+) #ffffff           |
| --cells-calendar-main-element-hover-bg-color                 | .main .element:hover                                                                        | background-color              |  transparent                                                                |
| --cells-calendar-main-element-hover-color                    | .main .element:hover                                                                        | color                         |  ![#121212](https://placehold.it/15/121212/000000?text=+) #121212           |
| --cells-calendar-main-element-hover-font-weight              | .main .element:hover                                                                        | font-weight                   |  700                                                                        |
| --cells-calendar-main-element-hover-transition               | .main .element:hover                                                                        | transition                    |  font-weight 0.3s ease-out                                                  |
| --cells-calendar-main-element-weekdays-bg-color              | .main .element.weekdays                                                                     | background-color              |  transparent                                                                |
| --cells-calendar-main-element-weekdays-color                 | .main .element.weekdays                                                                     | color                         |  ![#bdbdbd](https://placehold.it/15/bdbdbd/000000?text=+) #bdbdbd           |
| --cells-calendar-main-element-weekdays-font-weight           | .main .element.weekdays                                                                     | font-weight                   |  500                                                                        |
| --cells-calendar-main-element-weekdays-width                 | .main .element.weekdays                                                                     | width                         |  23px                                                                       |
| --cells-calendar-sm-container-height                         | @media screen and (min-width: 37.5em) > .container                                          | height                        |  370px                                                                      |
| --cells-calendar-sm-container-max-width                      | @media screen and (min-width: 37.5em) > .container                                          | max-width                     |  600px                                                                      |
| --cells-calendar-sm-container-width                          | @media screen and (min-width: 37.5em) > .container                                          | width                         |  380px                                                                      |
| --cells-calendar-sm-main-padding-left                        | @media screen and (min-width: 37.5em) > .main                                               | padding-left                  |  15px                                                                       |
| --cells-calendar-sm-main-padding-right                       | @media screen and (min-width: 37.5em) > .main                                               | padding-right                 |  15px                                                                       |
| --cells-calendar-sm-main-element-height                      | @media screen and (min-width: 37.5em) > .main .element                                      | height                        |  30px                                                                       |
| --cells-calendar-sm-main-element-line-height                 | @media screen and (min-width: 37.5em) > .main .element                                      | line-height                   |  30px                                                                       |
| --cells-calendar-sm-main-element-width                       | @media screen and (min-width: 37.5em) > .main .element                                      | width                         |  30px                                                                       |
| --cells-calendar-sm-main-element-days-width                  | @media screen and (min-width: 37.5em) > .main .element.days                                 | width                         |  30px                                                                       |
| --cells-calendar-sm-main-element-days-width                  | @media screen and (min-width: 37.5em) > .main .element.hiddenDay                            | width                         |  30px                                                                       |
| --cells-calendar-sm-main-element-months-width                | @media screen and (min-width: 37.5em) > .main .element.months                               | width                         |  100px                                                                      |
| --cells-calendar-sm-main-element-weekdays-width              | @media screen and (min-width: 37.5em) > .main .element.weekdays                             | width                         |  30px                                                                       |
### @apply
| Mixins                                           | Selector                                                                         | Value |
| ------------------------------------------------ | -------------------------------------------------------------------------------- | ----- |
| --layout                                         | :host                                                                            | {}    |
| --cells-calendar                                 | :host                                                                            | {}    |
| --layout-vertical                                | .container                                                                       | {}    |
| --cells-calendar-container                       | .container                                                                       | {}    |
| --layout-horizontal                              | .header                                                                          | {}    |
| --layout-around-justified                        | .header                                                                          | {}    |
| --cells-calendar-header                          | .header                                                                          | {}    |
| --layout                                         | .header .combo                                                                   | {}    |
| --layout-center                                  | .header .combo                                                                   | {}    |
| --cells-calendar-header-combo                    | .header .combo                                                                   | {}    |
| --cells-calendar-combo-box-input-container       | .header .combo vaadin-combo-box-light.combo-box > --paper-input-container:       | {}    |
| --cells-calendar-combo-box-input-container-input | .header .combo vaadin-combo-box-light.combo-box > --paper-input-container-input: | {}    |
| --layout                                         | .header .controls                                                                | {}    |
| --layout-center                                  | .header .controls                                                                | {}    |
| --layout-center-justified                        | .header .controls                                                                | {}    |
| --cells-calendar-header-controls                 | .header .controls                                                                | {}    |
| --cells-calendar-header-controls-prev-btn        | .header .controls .prev-btn                                                      | {}    |
| --cells-calendar-header-controls-title           | .header .controls .title                                                         | {}    |
| --cells-calendar-header-controls-next-btn        | .header .controls .next-btn                                                      | {}    |
| --layout-horizontal                              | .main                                                                            | {}    |
| --layout-wrap                                    | .main                                                                            | {}    |
| --layout-justified                               | .main                                                                            | {}    |
| --cells-calendar-main                            | .main                                                                            | {}    |
| --cells-calendar-main-elements                   | .main .element                                                                   | {}    |
| --cells-calendar-main-element-days               | .main .element.days                                                              | {}    |
| --cells-calendar-main-element-days-today         | .main .element.today                                                             | {}    |
| --cells-calendar-main-element-months             | .main .element.months                                                            | {}    |
| --cells-calendar-main-element-years              | .main .element.years                                                             | {}    |
| --cells-calendar-main-element-days-highlighted   | .main .element.day-highlighted-class                                             | {}    |
| --cells-calendar-main-element-selected           | .main .element.selected                                                          | {}    |
| --cells-calendar-main-element-hover              | .main .element:hover                                                             | {}    |
| --cells-calendar-main-element-weekdays           | .main .element.weekdays                                                          | {}    |
| --cells-calendar-sm-container                    | @media screen and (min-width: 37.5em) > .container                               | {}    |
| --cells-calendar-sm-main-elements                | @media screen and (min-width: 37.5em) > .main .element                           | {}    |
| --cells-calendar-sm-main-element-days            | @media screen and (min-width: 37.5em) > .main .element.days                      | {}    |
| --cells-calendar-sm-main-element-months          | @media screen and (min-width: 37.5em) > .main .element.months                    | {}    |
| --cells-calendar-sm-main-element-weekdays        | @media screen and (min-width: 37.5em) > .main .element.weekdays                  | {}    |
