(function() {

  'use strict';

  Polymer({

    is: 'cells-date-input',

    properties: {
      /**
       * Show a configurable text in the date.
       */
      text: {
        type: String
      },
      /**
       * Format date selected.
       */
      format: {
        type: String,
        value: 'DD/MM/YYYY'
      },
      /**
       * Actual Date.
       */
      date: {
        type: String,
        observer: 'setDate',
        value: function() {
          return this._formatDate(Date());
        }
      },
      /**
      * Date picker identifier
      */
      name: String,
      /**
       * Format date with locale
       */
      locale: {
        type: String,
        value: 'es'
      },
      /**
       * Minimum date allowed (YYYY-MM-DD or YYYY/MM/DD).
       */
      minDate: {
        type: String
      },
      /**
       * Maximum date allowed (YYYY-MM-DD or YYYY/MM/DD).
       */
      maxDate: {
        type: String
      },
      /**
       * Boolean attribute indicating whether the selection of dates in the past is allowed.
       */
      allowPastDate: {
        type: Boolean,
        value: false
      },
      /**
       * Default icon.
       */
      icon: {
        type: String,
        value: 'coronita:calendar'
      },
      /**
       * Default icon size.
       */
      iconSize: {
        type: Number,
        value: 24
      },
      /**
       * Boolean property setting weekend days availability
       */
      availableWeekends: {
        type: Boolean,
        value: false
      },
      /**
       * Boolean disabled select date
       */
      disabled: {
        type: Boolean,
        value: false,
        observer: '_setDisabled'
      },
      /**
       * Last date selected
       * @type {String}
       */
      _lastDate: {
        type: String
      }
    },

    /**
     * Reset state
     */
    reset: function() {
      this.date = this._formatDate(Date());
    },

    /**
     * set date after select in calendar
     */
    setDate: function(date, lastDate) {
      var today = new Date();
      var formatToday = this._formatDate(today);
      var error = false;
      var selectedUTCDay = new Date(date).getUTCDay();
      this.date = this._formatDate(date);
      if (!this._lastDate) {
        this._lastDate = formatToday;
      } else {
        this._lastDate = lastDate;
      }
      if ((new Date(date).getTime() < new Date(formatToday).getTime())  && !this.allowPastDate) {
        error = true;
        this._showError(error);
        this.dispatchEvent(new CustomEvent('input-date-error', {
          bubbles: true,
          composed: true,
          detail: {
            message: 'You have to select a date bigger than today.',
            error: 'past-date-not-allowed'
          }
        }));
      }
      if (!this.availableWeekends && (selectedUTCDay === 0 || selectedUTCDay === 6)) {
        error = true;
        this._showError(error);
        this.dispatchEvent(new CustomEvent('input-date-error', {
          bubbles: true,
          composed: true,
          detail: {
            message: 'Weekends not allowed',
            error: 'weekends-not-allowed'
          }
        }));
      }
      if (!error) {
        this._showError(error);
        this.blur();
        this.dispatchEvent(new CustomEvent('date-changed', {
          bubbles: true,
          composed: true,
          detail: this.date
        }));
      }
    },

    /**
     * show calendar on click in input
     */
    _showCalendar: function(e) {
      if (this.disabled) {
        e.preventDefault();
      } else {
        this.dispatchEvent(new CustomEvent('show-calendar', {
          detail: e,
          bubbles: true,
          composed: true
        }));
      }
    },

    _showError: function(error) {
      if (error) {
        this.classList.add('error');
      } else {
        this.classList.remove('error');
      }
    },

    _setDisabled: function(disabled) {
      if (disabled) {
        this.setAttribute('disabled', '');
      } else {
        this.removeAttribute('disabled');
      }
    },

    /*
     * format date to HTML5 standart
     */
    _formatDate: function(d) {
      var date = new Date(d);
      var dd = date.getDate();
      var mm = date.getMonth() + 1;
      var yyyy = date.getFullYear();

      if (dd < 10) {
        dd = '0' + dd;
      }

      if (mm < 10) {
        mm = '0' + mm;
      }
      var dateFormated = yyyy + '-' + mm + '-' + dd;
      return dateFormated;
    },

    /**
     * Fired when we select a past date or a weekend and they aren't allowed
     * @event input-date-error
     */
    /**
     * Fired after clicking the date input
     * @event show-calendar
     */
    /**
     * Fired after clicking the date input
     * @event date-changed
     */

  });

}());
