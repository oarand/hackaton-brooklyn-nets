(function() {

  'use strict';

  Polymer({

    is: 'cells-checkbox-group',

    properties: {
    /**
     * Array populated with the list of children checkbox buttons
     */
      allCheckboxes: {
        type: Array
      },
    /**
     * Checked icon to propagate to all children checkboxes
     */
      iconCheck: {
        type: String,
        observer: '_iconCheck'
      },
    /**
     * Unchecked icon to propagate to all children checkboxes
     */
      icon: {
        type: String,
        observer: '_icon'
      }
    },

    hostAttributes: {
      'role': 'group'
    },

    ready: function() {
      this.updateCheckboxes();
    },
    /**
     * Propagates group unchecked icon to each child checkbox
     */
    _icon: function() {
      this.async(function() {
        for (var i = 0; i < this.allCheckboxes.length; i++) {
          this.allCheckboxes[i].set('icon', this.icon);
        }
      });
    },
    /**
     * Propagates group checked icon to each child checkbox
     */
    _iconCheck: function() {
      this.async(function() {
        for (var i = 0; i < this.allCheckboxes.length; i++) {
          this.allCheckboxes[i].set('iconCheck', this.iconCheck);
        }
      });
    },
    /**
     * Updates array lists of the group with current checkbox buttons
     */
    updateCheckboxes: function() {
      this.allCheckboxes = Polymer.dom(this).querySelectorAll('cells-checkbox-button');
    }
  });
}());