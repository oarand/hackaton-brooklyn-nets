# cells-lodash-import

[![Certificated](https://img.shields.io/badge/certificated-yes-brightgreen.svg)](http://bbva-files.s3.amazonaws.com/cells/bbva-catalog/index.html)


[Demo of component in Cells Catalog](http://bbva-files.s3.amazonaws.com/cells/bbva-catalog/index.html#/elements/cells-lodash-import)

A simple wrapper for include the javascript utility library [Lodash](https://lodash.com/).

> __lodash__ version is ^4.17.4

## Usage

### Import the full build

```html
<link rel="import" href="cells-lodash-import.html">
```

### Import only the lodash core build

```html
<link rel="import" href="cells-lodash-core-import.html">
```
