from flask import Flask, request, jsonify
from flask_cors import CORS
from sklearn.externals import joblib
import datetime
import sys
import holidays
import numpy as np
app = Flask(__name__)
CORS(app)

PORT = 7000
HOST = '0.0.0.0'

MODEL_NAME = "./calls_model.pkl"
ERROR_MESSAGE = "Error"


def isHoliday (date):
    spain_holidays = holidays.ES()
    if date.weekday == 5 or date.weekday == 6 or (date in spain_holidays):
        return 1
    else:
        return 0
        

def formatDate (date, dateFormat):
    return datetime.datetime.strptime(date.strip(), dateFormat)

def getModelDate():
    dateObject = request.args.get('date')
    dateFormatted = formatDate(dateObject, '%Y-%m-%d')
    return [dateFormatted.year, dateFormatted.month, dateFormatted.day, dateFormatted.hour, dateFormatted.weekday(),
        isHoliday(dateFormatted)]
    
def getModelPrediction(model_data) -> list:
    model = joblib.load(MODEL_NAME)
    
    matriz = [[model_data[0], model_data[1], model_data[2], 0, model_data[3], model_data[4]]]
    for i in range(1,24):
        matriz = np.concatenate([matriz,[[model_data[0], model_data[1], model_data[2], i, model_data[3], model_data[4]]]])

    return model.predict(matriz)

@app.route('/predict_day', methods=['GET'])
def predictDay() -> list:
    model_date = getModelDate();
    
    model_prediction = getModelPrediction(model_date)
    response  = {
        'day': model_date[2],
        'month': model_date[1],
        'year': model_date[0],
        'calls': list(model_prediction[:,0]),
        'avg_time': list(model_prediction[:,1]),
        'avg_time_admin': list(model_prediction[:,2]),
        'avg_time_conversation': list(model_prediction[:,3]),
        'avg_time_retention': list(model_prediction[:,3]),
        'premium': list(model_prediction[:,5]),
        'avg_time_wait': list(model_prediction[:,6]),
    }

    return jsonify(response)
    

@app.route('/predict_waiting_time', methods=['GET'])
def predictWaitingTime() -> list:
    model_date = getModelDate();
    model_prediction = getModelPrediction(model_date)

    response  = {
        'avg_time_wait': list(model_prediction[:,6]),
    }

    return jsonify(response)
    
    
# Initialize the HTTP server.
if __name__ == '__main__':
    app.run(host=HOST, port=PORT, debug=False)
